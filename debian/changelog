liblocale-maketext-lexicon-perl (1.00-1.1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 22 Apr 2021 15:22:46 +0200

liblocale-maketext-lexicon-perl (1.00-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 28 Dec 2020 16:00:07 +0100

liblocale-maketext-lexicon-perl (1.00-1) unstable; urgency=medium

  * Import Upstream version 1.00 (closes: #724495)

 -- Florian Schlichting <fsfs@debian.org>  Fri, 07 Mar 2014 22:02:58 +0100

liblocale-maketext-lexicon-perl (0.99-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Import Upstream version 0.99

 -- Florian Schlichting <fsfs@debian.org>  Wed, 05 Mar 2014 23:04:32 +0100

liblocale-maketext-lexicon-perl (0.98-1) unstable; urgency=medium

  [ Florian Schlichting ]
  * Import Upstream version 0.97, 0.98
  * Add upstream-repo URL to Source field
  * Bump copyright years
  * Declare compliance with Debian Policy 3.9.5

 -- Florian Schlichting <fsfs@debian.org>  Wed, 29 Jan 2014 21:41:13 +0100

liblocale-maketext-lexicon-perl (0.96-1) unstable; urgency=low

  * Imported Upstream version 0.96.

 -- Florian Schlichting <fsfs@debian.org>  Thu, 08 Aug 2013 00:26:18 +0200

liblocale-maketext-lexicon-perl (0.94-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Imported Upstream version 0.94
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Deleted copyright paragraphs for inc/*, dropped upstream
  * Bumped Standards-Version to 3.9.4 (update to copyright-format 1.0)
  * Dropped version on suggested packages satisfied in oldstable

 -- Florian Schlichting <fsfs@debian.org>  Sat, 25 May 2013 22:08:40 +0200

liblocale-maketext-lexicon-perl (0.91-1) unstable; urgency=low

  [ Florian Schlichting ]
  * Imported Upstream version 0.91
  * Bump Standards-Version to 3.9.2 (no changes needed).
  * Depend on perl 5.14, which contains Locale::Maketext 1.19 >= 1.17
  * Add myself to Uploaders.

  [ Ansgar Burchardt ]
  * New upstream release.
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ Jonathan Yu ]
  * New upstream release
  * Bump to debhelper compat 8
  * Standards-Version 3.9.1 (specifically refer to GPL-1)
  * Refresh copyright information

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Fri, 18 Nov 2011 09:16:34 +0000

liblocale-maketext-lexicon-perl (0.82-1) unstable; urgency=low

  * New upstream release.
  * Drop patch: applied upstream.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Mon, 12 Apr 2010 23:40:02 +0900

liblocale-maketext-lexicon-perl (0.79-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release 0.78
  * Update copyright to new DEP5 format
  * Standards-Version 3.8.4 (drop perl version dep)
  * Add myself to Uploaders and Copyright
  * Rewrite control description

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * New upstream release 0.79.
  * Make some build dependencies versioned.
  * Minimize debian/rules.
  * Add patch to fix spelling mistakes.
  * Convert to source format 3.0 (quilt).

 -- gregor herrmann <gregoa@debian.org>  Sat, 06 Mar 2010 16:42:28 +0100

liblocale-maketext-lexicon-perl (0.77-1) unstable; urgency=low

  * New upstream release.
  * Add libppi-perl to Suggests and Build-Depends-Indep.
  * Install /usr/bin/xgettext.pl (closes: #508505).
    Add a lintian-override including the reasoning.
    Don't install the script as an example anymore.
  * debian/copyright: update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 01 Jan 2009 21:11:22 +0100

liblocale-maketext-lexicon-perl (0.75-1) unstable; urgency=low

  * New upstream release.
  * Remove patch (applied upstream), quilt framework and debian/README.source.
  * debian/copyright: add Upstream-Name field.
  * debian/control: 
    - Changed: Switched Vcs-Browser field to ViewSVN (source stanza).
    - Added: ${misc:Depends} to Depends: field.

 -- gregor herrmann <gregoa@debian.org>  Sun, 30 Nov 2008 18:35:13 +0100

liblocale-maketext-lexicon-perl (0.73-1) unstable; urgency=low

  [ Jose Luis Rivas ]
  * New upstream release
  * debian/watch: Fixed, now there's only one URL from where to fetch
  the tarball, the dist-based.
  * Added me as Uploader.
  * Refreshed debian/copyright.

  [ gregor herrmann ]
  * debian/control:
    - add libtest-pod-perl to enable an additional test
    - mention module name in long description
    - add packages for recommended modules to Suggests:

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Mon, 03 Nov 2008 23:32:53 -0430

liblocale-maketext-lexicon-perl (0.71-1) unstable; urgency=low

  * New upstream release
  * drop 02more_TT_whitespace_flags.patch; upstream now uses a full-scale TT2
    parser
  * 01needs_chomp.patch refreshed
  * add libyaml-perl and libtemplete-perl to B-D-I enabling additional tests

 -- Damyan Ivanov <dmn@debian.org>  Tue, 14 Oct 2008 23:07:26 +0300

liblocale-maketext-lexicon-perl (0.68-2) unstable; urgency=low

  * add 02more_TT_whitespace_flags.patch, adding support to all Template
    Toolkit tag modifiers, not only [%- -%]
  * add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Wed, 10 Sep 2008 18:13:37 +0300

liblocale-maketext-lexicon-perl (0.68-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Aug 2008 12:39:27 -0300

liblocale-maketext-lexicon-perl (0.67-1) unstable; urgency=low

  * New upstream release.
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * Set Standards-Version to 3.8.0 (no further changes).
  * debian/watch: add author-based URL, dist-based doesn't have the newest
    version.
  * debian/control: change my email address.
  * debian/copyright: add information about files under inc/Module/.

 -- gregor herrmann <gregoa@debian.org>  Mon, 11 Aug 2008 21:13:53 -0300

liblocale-maketext-lexicon-perl (0.66-1) unstable; urgency=low

  * New upstream release.
  * debian/rules:
    - delete /usr/lib/perl5 only if it exists
    - remove configure{,-stamp} targets, create Makefile in build-stamp
      target and remove *VENDOR* variables
    - remove --destdir from dh_builddep call
    - use DESTDIR and PREFIX on make install
    - remove dh_installdirs call
    - don't install README any more, it's now just a text version of the POD
    - use $(TMP) more often where possible
    - remove some more empty directories
  * debian/watch: extend regexp for matching upstream tarballs.
  * Set debhelper compatibility level to 6.
  * debian/copyright:
    - update years of copyright
    - add additional copyright holders
    - convert to new format
  * Drop Debian revision from versioned build dependency, thanks lintian.
  * Add /me to Uploaders.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 17 Feb 2008 23:49:49 +0100

liblocale-maketext-lexicon-perl (0.65-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release.
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/watch: use dist-based URL.
  * Adapt patch 02ignore_nonexistant to new upstream code.
  * Set Standards-Version to 3.7.3 (no changes needed).
  * debian/copyright: update years of copyright, indent verbatim copy of MIT
    license.
  * debian/rules: introduce install-stamp, make sure that patching occurs
    before configure, call dh_clean before make distclean, remove
    dh_installman.

  [ Niko Tyni ]
  * Migrate from dpatch to quilt for patch management.
  * debian/patches/02ignore_nonexistant: removed, #372124 was fixed
    in upstream 0.63 in a different way.
  * Change to my @debian.org email address in the Uploaders field.
  * Some more small tweaks to debian/rules.

 -- Niko Tyni <ntyni@debian.org>  Thu, 27 Dec 2007 22:20:16 +0200

liblocale-maketext-lexicon-perl (0.62-2) unstable; urgency=low

  * New maintainer.
  * Upgrade to debhelper compatibility level 5.
  * Version the dpatch dependency, for /usr/share/dpatch/dpatch-run .

 -- Niko Tyni <ntyni@iki.fi>  Tue,  5 Dec 2006 00:13:33 +0200

liblocale-maketext-lexicon-perl (0.62-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Sun, 16 Jul 2006 09:06:36 +0100

liblocale-maketext-lexicon-perl (0.61-2) unstable; urgency=low

  * Added patch 02non-existant to fix "fails on non-existent PO files",
    closes: #372124

 -- Stephen Quinney <sjq@debian.org>  Thu,  8 Jun 2006 14:29:39 +0100

liblocale-maketext-lexicon-perl (0.61-1) unstable; urgency=low

  * New upstream release
  * debian/copyright
      - Another email address change for upstream
      - Now under the MIT license

 -- Stephen Quinney <sjq@debian.org>  Thu,  4 May 2006 15:47:44 +0100

liblocale-maketext-lexicon-perl (0.60-1) unstable; urgency=low

  * New upstream release
  * debian/copyright updated to match new email address for upstream.

 -- Stephen Quinney <sjq@debian.org>  Sun, 16 Apr 2006 18:31:04 +0100

liblocale-maketext-lexicon-perl (0.53-2) unstable; urgency=low

  * Locale/Maketext/Extract/Run.pm - patched to fix issue "::Run -f option
    non-working, closes: #307777, thanks to Damyan Ivanov for the patch.
  * Added build dependency on dpatch.

 -- Stephen Quinney <sjq@debian.org>  Fri, 10 Feb 2006 14:56:59 +0000

liblocale-maketext-lexicon-perl (0.53-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Mon,  2 Jan 2006 10:02:08 +0000

liblocale-maketext-lexicon-perl (0.49-1) unstable; urgency=high

  * New upstream release - bug fix - now less eager to die on non-existant
    files. This fix is important as the request-tracker packages need it
    to function correctly therefore urgency is high to get into Sarge.

 -- Stephen Quinney <sjq@debian.org>  Thu, 14 Apr 2005 08:40:34 +0100

liblocale-maketext-lexicon-perl (0.48-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Fri, 18 Mar 2005 09:12:37 +0000

liblocale-maketext-lexicon-perl (0.47-1) unstable; urgency=low

  * New upstream release - fix for possible deep recursion problems.

 -- Stephen Quinney <sjq@debian.org>  Mon,  7 Feb 2005 12:12:49 +0000

liblocale-maketext-lexicon-perl (0.46-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Thu, 16 Dec 2004 19:02:50 +0000

liblocale-maketext-lexicon-perl (0.45-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Tue, 26 Oct 2004 20:09:52 +0100

liblocale-maketext-lexicon-perl (0.44-1) unstable; urgency=low

  * New upstream release
  * debian/watch - updated to point to www.cpan.org instead of
    search.cpan.org so uscan might not fail quite so often.

 -- Stephen Quinney <sjq@debian.org>  Sat, 25 Sep 2004 16:55:46 +0100

liblocale-maketext-lexicon-perl (0.42-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Sat, 28 Aug 2004 10:16:52 +0100

liblocale-maketext-lexicon-perl (0.40-1) unstable; urgency=low

  * New upstream release - Small changes, mainly Win32 related.

 -- Stephen Quinney <sjq@debian.org>  Tue, 24 Aug 2004 20:03:05 +0100

liblocale-maketext-lexicon-perl (0.38-1) unstable; urgency=low

  * New upstream release - fix the bug fix from the last release also adds
    some new functionality.

 -- Stephen Quinney <sjq@debian.org>  Tue, 27 Apr 2004 19:34:17 +0100

liblocale-maketext-lexicon-perl (0.37-1) unstable; urgency=low

  * New upstream release - small bug fix.

 -- Stephen Quinney <sjq@debian.org>  Thu, 22 Apr 2004 19:51:05 +0100

liblocale-maketext-lexicon-perl (0.36-1) unstable; urgency=low

  * New upstream release - bug fixes and some new features.

 -- Stephen Quinney <sjq@debian.org>  Sat, 20 Mar 2004 09:43:34 +0000

liblocale-maketext-lexicon-perl (0.35-1) unstable; urgency=low

  * New upstream release.
  * No longer need to depend on Regexp::Common.

 -- Stephen Quinney <sjq@debian.org>  Fri, 20 Feb 2004 15:55:28 +0000

liblocale-maketext-lexicon-perl (0.34-1) unstable; urgency=low

  * New upstream release - note that, in particular, this fixes some
    issues with files that have DOS/Windows-type line-endings.

 -- Stephen Quinney <sjq@debian.org>  Fri,  2 Jan 2004 13:33:37 +0000

liblocale-maketext-lexicon-perl (0.33-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Tue, 16 Dec 2003 10:55:02 +0000

liblocale-maketext-lexicon-perl (0.32-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Fri, 17 Oct 2003 14:06:21 +0100

liblocale-maketext-lexicon-perl (0.29-1) unstable; urgency=low

  * New upstream release.

 -- Stephen Quinney <sjq@debian.org>  Fri, 10 Oct 2003 16:20:19 +0100

liblocale-maketext-lexicon-perl (0.28-1) unstable; urgency=low

  * New upstream release.

 -- Stephen Quinney <sjq@debian.org>  Mon,  1 Sep 2003 16:51:12 +0100

liblocale-maketext-lexicon-perl (0.27-1) unstable; urgency=low

  * New upstream release - bug fixes.
  * Updated to policy version 3.6.0 - no changes needed.

 -- Stephen Quinney <sjq@debian.org>  Fri, 25 Jul 2003 11:45:27 +0100

liblocale-maketext-lexicon-perl (0.26-1) unstable; urgency=low

  * New upstream release.

 -- Stephen Quinney <sjq@debian.org>  Tue, 20 May 2003 13:35:06 +0100

liblocale-maketext-lexicon-perl (0.24-1) unstable; urgency=low

  * New upstream release - bug fixes.

 -- Stephen Quinney <sjq@debian.org>  Tue, 29 Apr 2003 16:56:14 +0100

liblocale-maketext-lexicon-perl (0.16-1) unstable; urgency=low

  *  Inital release, closes: #188329.

 -- Stephen Quinney <sjq@debian.org>  Thu, 17 Apr 2003 11:10:27 +0100


